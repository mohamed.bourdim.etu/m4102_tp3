/pizzas 					GET		Renvoie la liste des 									pizzas.

/pizzas/{id}  				GET  		Renvoie une pizza en 									particulier.

/pizzas/{id}/ingredients  		GET		Renvoie la liste des 									ingr�dients de la 									pizza.

/pizzas   					POST		Cr�e une nouvelle 									pizza.

/pizzas/{id}   				DELETE	Supprime une pizza en 								particulier de la 									liste de pizzas.



{
"id" : "f38806a8-7c85-49ef-980c-149dcd81d306",
"name" : "Calzone",
"ingr�dients" : "Mozarella","jambon","champignon"
}

